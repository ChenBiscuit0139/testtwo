const request = require('supertest');
const sinon = require('sinon');
const server = require('../server.js');

describe('server.js', function() {
  describe('GET /', function() {
    it('displays "Hello World!"', function(done) {
      // The line below is the core test of our app.
      request(server.app)
        .get('/')
        .expect(function(res) {
          if (res.text.indexOf('Hello World!') === -1)
            throw new Error('Missing Hello World!');
        })
        .expect(200, done);
    });
  });
  describe('func', function() {
    it('should call callback', function() {
      const clock = sinon.useFakeTimers();
      const spy = sinon.spy();
      server.func(spy);
      clock.tick(1000);
      sinon.assert.called(spy);
    });
    it('should call twice callback', function() {
      const clock = sinon.useFakeTimers();
      const spy = sinon.spy();
      server.func(spy);
      clock.tick(1000);
      sinon.assert.calledTwice(spy);
    });
  });
});
