const express = require('express');

const app = express();

// The code below will display 'Hello World How is it going!' to the browser when you go to http://localhost:3000
app.get('/', function(req, res) {
  res.send('Hello World!');
});

const port = process.env.PORT || '3000';

app.listen(port);

module.exports.app = app;

module.exports.func = function func(callback) {
  setTimeout(() => {
    callback();
    callback();
  }, 1000);
};
